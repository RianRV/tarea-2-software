//TAREA 2 - SOFTWARE

import ddf.minim.*;
Minim minim;
AudioPlayer player;

int circleX_1, circleY_1;
int circleX_2, circleY_2;
int circleSize = 100;

boolean circleOver_1 = false;
boolean circleOver_2 = false;

int t = 0;
int timer = 100;
int nextTime = 0;
int previousTime = 0;

void setup()
  {
    size(640, 480);
    smooth();
    noStroke();
  
    minim = new Minim(this);
    player = minim.loadFile("Audio.mp3");
  
    circleX_1 = 150;
    circleY_1 = 380;
    circleX_2 = 490;
    circleY_2 = 380;
    rectMode(CENTER);
  }

void draw()
  {
    update(mouseX, mouseY);
    background(8,51,162);
    fill(150);
    stroke(0,0,0);
    rect(320, 380, 220, 60);
  
    if (circleOver_1)
      {
        fill(152,255,150);
      }
    else
      {
        fill(0,52,0);
      }
    stroke(0,0,0);
    circle(150, 380, 100);
  
    if (circleOver_2)
      {
        fill(255,0,0);
      }
    else
      {
        fill(127,0,0);
      }
    stroke(0,0,0);
    circle(490, 380, 100);
    
    
    //Aceleración
    if (circleOver_1)
      {
        if (millis()-previousTime>= timer)
          {
            previousTime = millis();
            t += 1;
            println(t);
            if (t >= 100)
              {
                t = 100;
              }
          }
      }
    
    //Frenado (desaceleración rapida)
    if (circleOver_2)
      {
        if (millis()-previousTime>= timer)
          {
            previousTime = millis();
            t -= 5;
            println(t);
            if (t < 0)
              {
                t = 0;
              }
          }
      }
      
    //Desaceleración normal
    if (!circleOver_1 && !circleOver_2)
      {
        if (millis()-previousTime>= timer)
          {
            previousTime = millis();
            println(t);
            t -= 1;
            if (t <= 0)
              {
                t = 0;
              }
          }
      }

    float t_map = map(t, 0, 100, 0, 200);
    float r = map(t, 0, 100, 0, 255);
    float g = map(t, 0, 100, 255, 0);
  
    if (t >= 100)
      {
        t_map = 200;
      }
    
    if (t <= 0)
      {
        t_map = 0;
      }
    
    fill(r,g,0);
    stroke(0,0,0);
    circle(320, 180, 300);
    
    fill(8,51,162);
    stroke(0,0,0);
    circle(320, 180, 250);
    
    fill(150);
    stroke(0,0,0);
    circle(320, 180, 50+t_map);
    
    textSize(24);
    textAlign(CENTER);
    fill(0,255,0);
    text("Velocidad:", 320, 375);
    text("[Km/h]", 350, 400);
    text(t, 280, 400);
    
    if (t == 100)
      {
        textSize(24);
        textAlign(CENTER);
        fill(255, 0, 0);
        text("VELOCIDAD MAXIMA", 320, 180);
      }
    
    if (t == 100)
      {
        player.play();
      }
  
    if (t == 99)
      {
        player.rewind();
      }
  
    if (t != 100)
      {
        player.pause();
      }
  }

void update(int x, int y)
  {
    if ( overCircle_1(circleX_1, circleY_1, circleSize) )
      {
        circleOver_1 = true;
        circleOver_2 = false;
      }
    else if ( overCircle_2(circleX_2, circleY_2, circleSize) )
      {
        circleOver_2 = true;
        circleOver_1 = false;
      }
    else
      {
        circleOver_1 = circleOver_2 = false;
      }
  }

boolean overCircle_1(int x, int y, int diameter)
  {
    float disX = x - mouseX;
    float disY = y - mouseY;
    if (sqrt(sq(disX) + sq(disY)) < diameter/2 )
      {
        return true;
      }
    else
      {
        return false;
      }
  }

boolean overCircle_2(int x, int y, int diameter)
  {
    float disX = x - mouseX;
    float disY = y - mouseY;
    if (sqrt(sq(disX) + sq(disY)) < diameter/2 )
      {
        return true;
      } 
    else
      {
        return false;
      }
  }
